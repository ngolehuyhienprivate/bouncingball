package com.example.admin.bouncingball;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Handle {
    private int x, y, width, height;

    public Handle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void drawHandle (Canvas canvas, Paint paint) {
        canvas.drawRect(new Rect(x, y, x + width, y + height), paint);
    }
}
