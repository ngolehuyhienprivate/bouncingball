package com.example.admin.bouncingball;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MyView extends View implements Runnable {

    private int x2=100, y2=100, dx2=20, dy2=20;
    private int x1=150, y1=300, dx1=10, dy1=10;
    Paint paint;
    int radius = 80;
    int vary = 72;
    Bitmap ballResize1, ballResize2, wallpaper, block;
    private int xHandle = 10, yHandle = 20, width = 160, height = 15;
    Ball ballOne, ballTwo;
    Handle handle;
    ArrayList<Brick> lists;
    Brick brick;
    SoundManager sound;
    private int point;
    String result;


    public MyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        ballOne = new Ball (x1, y1, dx1, dy1, radius);
        ballTwo = new Ball (x2, y2, dx2, dy2, radius);
        Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.balla);
        ballResize1 = Bitmap.createScaledBitmap(ball,radius,radius,false);
        Bitmap ball2 = BitmapFactory.decodeResource(getResources(), R.drawable.ballb);
        ballResize2 = Bitmap.createScaledBitmap(ball2,radius,radius,false);
        Bitmap wall = BitmapFactory.decodeResource(getResources(), R.drawable.wall);
        wallpaper = Bitmap.createScaledBitmap(wall,820,1080,false);
        handle = new Handle(xHandle, yHandle, width, height);
        Bitmap blockbrick = BitmapFactory.decodeResource(getResources(), R.drawable.brick);
        block = Bitmap.createScaledBitmap(blockbrick,105,70,false);
        sound = SoundManager.getInstance();
        sound.init(context);
        result = "0";

        lists = new ArrayList<Brick>();

        for (int i = 0; i < 7; i ++){
            brick = new Brick (110*i, 0, 105, 70, true);
            lists.add(brick);
        }

        for (int i = 0; i < 7; i ++){
            brick = new Brick (110*i, 75, 105, 70, true);
            lists.add(brick);
        }

        for (int i = 0; i < 7; i ++){
            brick = new Brick (110*i, 150, 105, 70, true);
            lists.add(brick);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);


        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
//        paint.setColor(Color.WHITE);
//        canvas.drawPaint(paint);

        canvas.drawBitmap(wallpaper, 0, 0, null);
        ballOne.drawBall(canvas, ballResize1);
//        ballOne.drawBall(canvas, paint, "#3e70c1");

        ballTwo.drawBall(canvas, ballResize2);
//        ballTwo.drawBall(canvas, paint, "#CD5C5C");


        paint.setColor(Color.parseColor("#a58718"));
        handle.drawHandle(canvas, paint);




        for(Brick element : lists){
            element.drawBrick(canvas, block);


        }



        if (isFinish() == false) {
            ballOne.moving();
            ballTwo.moving();
            ballOne.checkTouchingBoundary(this.getWidth(), this.getHeight());
            ballTwo.checkTouchingBoundary(this.getWidth(), this.getHeight());
            ballOne.hitAnotherBall(ballTwo);
            ballOne.checkTouchHandle(handle);
            ballTwo.checkTouchHandle(handle);


            for (Brick element : lists) {

                if (ballOne.checkTouchingWithBrick(element)){
                    ballOne.checkTouchingWithBrick(element);
                    sound.playSound(R.raw.hit);
                    point +=10;
                }
                if (ballTwo.checkTouchingWithBrick(element)){
                    ballTwo.checkTouchingWithBrick(element);
                    sound.playSound(R.raw.hit);
                    point +=10;
                }
                result = String.valueOf(point);
                paint.setColor(Color.YELLOW);
                paint.setTextSize(50);
                canvas.drawText(result,30,930,paint);
            }

        } else {
            paint.setColor(Color.DKGRAY);
            paint.setTextSize(150);
            canvas.drawText("You won!", 100, 500, paint);
            sound.playSound(R.raw.finish);
        }
        invalidate();
    }



    @Override
    protected  void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
        handle.setX(this.getWidth() / 2 - handle.getWidth()/2);
        handle.setY(this.getHeight()- 100);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        boolean handled = false;

        int xTouch;
        int yTouch;
        int actionIndex = event.getActionIndex();


        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:

                xTouch = (int) event.getX(0);
                yTouch = (int) event.getY(0);



                handled = true;
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                xTouch = (int) event.getX(actionIndex);
                yTouch = (int) event.getY(actionIndex);


                handled = true;
                break;

            case MotionEvent.ACTION_MOVE:
                final int pointerCount = event.getPointerCount();

                for (actionIndex = 0; actionIndex < pointerCount; actionIndex++) {

                    xTouch = (int) event.getX(actionIndex);
                    yTouch = (int) event.getY(actionIndex);

                    handle.setX(xTouch);

                }


                invalidate();
                handled = true;
                break;

            case MotionEvent.ACTION_UP:

                invalidate();
                handled = true;
                break;

            case MotionEvent.ACTION_POINTER_UP:

                invalidate();
                handled = true;
                break;

            case MotionEvent.ACTION_CANCEL:

                handled = true;
                break;

            default:
                // do nothing
                break;
        }

        return super.onTouchEvent(event) || handled;
    }

    public  boolean isFinish(){
        for(Brick element : lists){
            if(element.getVisibility()){
                return false;
            }
        }
        return true;
    }

    @Override
    public void run() {

    }
}