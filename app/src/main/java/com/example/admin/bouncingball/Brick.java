package com.example.admin.bouncingball;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class Brick {
    private int x, y, width, height;
    private  boolean isVisible;

    public Brick(int x, int y, int width, int height, boolean isVisible) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.isVisible = isVisible;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void drawBrick (Canvas canvas, Paint paint){
        if (isVisible){
            canvas.drawRect(new RectF(x, y, x + width, y + height), paint);
        }
    }

    public void drawBrick (Canvas canvas, Bitmap bitmap){
        if (isVisible){
            canvas.drawBitmap(bitmap, x, y, null);
        }
    }

    public void setInvisible() {
        isVisible = false;
    }

    public boolean getVisibility() {
        return isVisible;
    }
}
